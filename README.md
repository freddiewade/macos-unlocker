# macOS Unlocker

To run our macOS High Sierra on VMware, we first need to patch the WMware program. I start the process, assuming that the program is installed on your computer. We download and patch VMware Unlocker software from the required files section below.

Required Files

1) macOS High Sierra image file
2) [VMware Unlocker](https://www.sysnettechsolutions.com/en/download-vmware-unlocker-2-1-1/) (Applies to all VMware versions 11-12-13-14-15.)

Let's extract the file we downloaded from rar and run the "win-install" file. If it gives an error, right-click and Run as "Administrator".

VM Unlocker program started patching for macOS. As a result of this short process, the command prompt closes.

After the patch process, we check the VMware program, if the Apple Mac OS option has come, it means the process was successful.

We click on Create a New Virtual Machine. I added the next steps as slides.

Note: After the above operations, right-click on the file with the extension “vmx” which you have specified in the place where the installation will be done and open it in the text document. Add **smc.version = "0"** to the last line of the file you opened and save it.

After performing the above steps, let's add our downloaded macOS High Sierra ISO file to CD DVD.

As a result of all these settings, we can give the start of the macOS High Sierra.